# REQUETE ET RANKING EN PYTHON

Contributeur : Pierre Vasseur

## Explication du code

opener.py sert à créer une instance python en liste d'un fichier au format json

tokenizer.py a pour rôles de tokenizer une requête donnée, et de retourner justement la liste de ces tokens

filterer.py permet de filter l'ensemble des documents selon la présence ou non de mots en commun entre leur titre et la requête, grâce à l'index, et retourne un dictionnaire avec pour clé son identifiant et pour valeur son importance (ici représentée par le nombre d'occurence des mots de la requête dans son titre).

selecter.py permet de conserver uniquement les documents dont l'identifiant a été conservé après filtrage

merger.py est utile pour ajouter l'importance du document dans la liste précédente

sorter.py permet de trier les documents de la liste filtrée par leur importance

formater.py sert à mettre en forme les documents filtrés et ordonnés en accord avec la consigne


## Execution

il suffit de lancer le fichier main.py pour créer le résultat d'une requête parmi un ensemble de documents à partir d'une requête, d'un index et d'un fichier de documents appelé "results.json"
un fichier json appelé "metadata.json" est également créé par la même exécution et fournit différentes statistiques sur les documents.