import json

from request.opener import openFile
from request.tokenizer import tokenizeRequest
from request.filterer import filterDocs
from request.selecter import selectDocs
from request.merger import mergeDict
from request.sorter import sortDocs
from request.formater import formatList

def main(request, documentsjson, indexjson):

    index = openFile(indexjson)
    documents = openFile(documentsjson)
    
    tokens = tokenizeRequest(request)

    dict = filterDocs(index, tokens)

    selecteddocs = selectDocs(documents, list(dict.keys()))

    mergeddocs = mergeDict(selecteddocs, dict)

    sorteddocs = sortDocs(mergeddocs)

    docs = formatList(sorteddocs)

    with open("results.json", "w") as f:
        json.dump(docs, f)
    
    docsinindex = []
    for value in index.values():
        keys = list(value.keys())
        docsinindex += keys
    docsinindex = set(docsinindex)

    nb_docs = len(docsinindex)
    nb_filtered = len(list(dict.keys()))

    statistics = {"num docs in index": nb_docs, "num filtered docs": nb_filtered}

    with open("metadata.json", "w") as f:
        json.dump(statistics, f)



if __name__ == "__main__":
    main("wikipédia site officiel définition dictionnaire cinémas primaire voiture", "documents.json", "index.json")