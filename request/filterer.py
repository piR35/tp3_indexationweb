
def filterDocs(index, tokens):
    dict = {}
    for token in tokens:
        for key, value in index.items():
            if token == key:
                for k, v in value.items():
                    if k in dict:
                        dict[k] += v["count"]
                    else:
                        dict[k] = v["count"]
    return dict
    
