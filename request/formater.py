
def formatList(doclist):
    formateddocs = []
    for doc in doclist:
        title, url = doc["title"], doc["url"]
        dict = {"title": title, "url": url}
        formateddocs.append(dict)
    return formateddocs