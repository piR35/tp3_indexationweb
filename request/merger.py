
def mergeDict(docs, dic):
    for doc in docs:
        for key, value in dic.items():
            if str(doc["id"]) == key:
                doc["importance"] = value
    return docs