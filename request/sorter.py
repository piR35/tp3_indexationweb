
def sortDocs(docs):
    docs.sort(key = lambda x: x.get('importance'))
    return docs
