from nltk.tokenize import word_tokenize

def tokenizeRequest(request):
    list = []
    tokens = word_tokenize(request)
    for token in tokens:
        list.append(token.lower())
    return list