from request.filterer import filterDocs

def testFilterDocs():
    #GIVEN
    index = {"nom1": {"ida": {"truc":1, "machin":2}, "idb": {"truc":6, "machin":9}},
    "nom2": {"idb": {"truc":5, "machin":7}, "idc": {"truc":4, "machin":3}},
    "nom3": {"idc": {"truc":2, "machin":2}, "ida": {"truc":8, "machin":8}}
    }
    tokens = ["nom3", "nom1"]
    #WHEN
    d = filterDocs(index, tokens)
    #THEN
    assert isinstance(d, dict)

if __name__ == "__main__":
    testFilterDocs()
    print("test passed")