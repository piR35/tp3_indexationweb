from request.formater import formatList

def testFormatList():
    #GIVEN
    docs = [{"id":3, "importance":100},{"id":4, "importance":75},{"id":2, "importance":50},{"id":1, "importance":25}]
    #WHEN
    documents = formatList(docs)
    #THEN
    assert list(documents.keys()) == ["title", "url"]

if __name__ == "__main__":
    testFormatList()
    print("test passed")