from request.merger import mergeDict

def testMergeDict():
    #GIVEN
    docs = [{"id":1, "truc":8},{"id":2, "truc":5},{"id":3, "truc":66},{"id":4, "truc":56}]
    dic = {"1":25, "2":50, "3": 100, "4":75}
    #WHEN
    d = mergeDict(docs, dic)
    #THEN
    assert "importance" in d

if __name__ == "__main__":
    testMergeDict()
    print("test passed")

