from request.opener import openFile

def testOpenFile(jsonfile, type):
    #GIVEN
    openfile = openFile(jsonfile)
    #WHEN
    #THEN
    return isinstance(openfile, type)

if __name__ == "__main__":
    testOpenFile("documents.json", list)
    testOpenFile("index.json", dict)
    print("test passed")