from request.selecter import selectDocs

def testSelectDocs():
    #GIVEN
    docs = [{"id":1, "truc":8},{"id":2, "truc":5},{"id":3, "truc":66},{"id":4, "truc":56}]
    identifiers = ["2", "4"]
    #WHEN
    documents = selectDocs(docs, identifiers)
    #THEN
    assert len(documents) == len(identifiers)

if __name__ == "__main__":
    testSelectDocs()
    print("test passed")
