from request.sorter import sortDocs

def testSortDocs():
    #GIVEN
    docs = [{"id":1, "importance":25},{"id":2, "importance":50},{"id":3, "importance":100},{"id":4, "importance":75}]
    #WHEN
    documents = sortDocs(docs)
    #THEN
    assert documents[0]["importance"] > documents[1]["importance"]

if __name__ == "__main__":
    testSortDocs()
    print("test passed")

