from request.tokenizer import tokenizeRequest

def testTokenizeRequest(request):
    #GIVEN
    tokens = tokenizeRequest(request)
    #WHEN
    #THEN
    assert isinstance(tokens, list)

if __name__ == "__main__":
    testTokenizeRequest("solution tp3 indexation web")
    print("test passed")